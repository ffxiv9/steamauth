using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MihaZupan;

namespace SteamAuth
{
    public class SteamWeb
    {
        public static string MOBILE_APP_USER_AGENT = "Dalvik/2.1.0 (Linux; U; Android 9; Valve Steam App Version/3)";

        public static async Task<string> RequestAsync(string url, string method, NameValueCollection data = null,
            CookieContainer cookies = null, NameValueCollection headers = null,
            string referer = APIEndpoints.COMMUNITY_BASE, string proxy = null, HttpContent content = null)
        {
            ProxyInfo proxyInfo = ParseProxy(proxy);

            HttpRequestMessage request = CreateRequest(url, method, content, data, headers, referer);

            HttpToSocks5Proxy socks5Proxy = new HttpToSocks5Proxy(new[] {proxyInfo});
            using (HttpClientHandler handler = new HttpClientHandler
                   {
                       Proxy = socks5Proxy,
                       UseProxy = true,
                       CookieContainer = cookies ?? new CookieContainer(),
                       UseCookies = true
                   })
            {
                try
                {
                    using (HttpClient client = new HttpClient(handler, true))
                    {
                        HttpResponseMessage response = await client.SendAsync(request).ConfigureAwait(false);
                        if (response.IsSuccessStatusCode)
                        {
                            return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        }

                        if (response.StatusCode == HttpStatusCode.Found)
                        {
                            return await RequestAsync(response.Headers.Location.AbsoluteUri, method, data, cookies, headers, referer, proxy).ConfigureAwait(false);
                        }

                        if (url.Contains(APIEndpoints.COMMUNITY_BASE))
                        {
                            return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        }

                        //todo handle
                        return null;
                    }
                }
                catch
                {
                    return null; //Change later, catch-alls are bad!
                }
                finally
                {
                    socks5Proxy.StopInternalServer();
                }
            }
        }

        private static HttpRequestMessage CreateRequest(string url, string method, HttpContent content,
            NameValueCollection data, NameValueCollection headers, string referer)
        {
            string query = data == null
                ? string.Empty
                : string.Join("&",
                    Array.ConvertAll(data.AllKeys,
                        key => $"{WebUtility.UrlEncode(key)}={WebUtility.UrlEncode(data[key])}"));

            if (method == "GET" && !string.IsNullOrWhiteSpace(query))
            {
                url += (url.Contains("?") ? "&" : "?") + query;
            }

            HttpRequestMessage request = new HttpRequestMessage(new HttpMethod(method), url);
            if (url.ToLower().StartsWith("https"))
            {
                request.Version = HttpVersion.Version11;
            }

            request.Headers.Accept.ParseAdd("text/javascript, text/html, application/xml, text/xml, */*");
            request.Headers.UserAgent.ParseAdd(MOBILE_APP_USER_AGENT);
            request.Headers.Referrer = new Uri(referer);

            if (headers != null)
            {
                foreach (string key in headers.AllKeys)
                {
                    request.Headers.Add(key, headers.Get(key));
                }
            }

            if (method == "POST" && data != null)
            {
                request.Content = content ?? new FormUrlEncodedContent(data.AllKeys.ToDictionary(k => k, k => data[k]));
            }

            return request;
        }

        public static ProxyInfo ParseProxy(string proxy)
        {
            if (string.IsNullOrWhiteSpace(proxy))
            {
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(proxy));
            }

            proxy = proxy.Replace(";", ":");

            string[] args = proxy.Split(':');
            if (!new[] {2, 4}.Contains(args.Length))
            {
                throw new InvalidOperationException($"{nameof(proxy)} is invalid");
            }

            if (!int.TryParse(args[1], out int port) || port <= 0 || port > ushort.MaxValue)
            {
                throw new InvalidOperationException($"{nameof(proxy)} port is invalid");
            }

            return args.Length == 2
                ? new ProxyInfo(args[0], port)
                : new ProxyInfo(args[0], port, args[2], args[3]);
        }
    }
}
