﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SteamAuth
{
    /// <summary>
    /// Handles logging the user into the mobile Steam website. Necessary to generate OAuth token and session cookies.
    /// </summary>
    public class UserLogin
    {
        public string Username;
        public string Password;
        public ulong SteamID;

        public bool RequiresCaptcha;
        public string CaptchaGID = null;
        public string CaptchaText = null;

        public bool RequiresEmail;
        public string EmailDomain = null;
        public string EmailCode = null;

        public bool Requires2FA;
        public string TwoFactorCode = null;

        public SessionData Session = null;

        private readonly CookieContainer _cookies = new CookieContainer();

        /// <summary>
        ///     Socks5 Proxy. String must be in one of this format
        ///     host:port
        ///     - or -
        ///     host:port:username
        ///     - or -
        ///     host:port:username:password
        /// </summary>
        public string Proxy;

        public UserLogin(string username, string password, string proxy = null)
        {
            Username = username;
            Password = password;
            Proxy = proxy;
        }

        public async Task<LoginResult> DoLoginAsync()
        {
            NameValueCollection postData = new NameValueCollection();
            CookieContainer cookies = _cookies;
            string response = null;

            if (cookies.Count == 0)
            {
                //Generate a SessionID
                cookies.Add(new Cookie("Steam_Language", "english", "/", ".steamcommunity.com"));

                await SteamWeb.RequestAsync(APIEndpoints.COMMUNITY_BASE, "GET", cookies:cookies, proxy:Proxy);
            }

            CookieCollection readableCookies = cookies.GetCookies(new Uri(APIEndpoints.COMMUNITY_BASE));

            if (readableCookies["sessionid"]?.Value == null)
                return LoginResult.GeneralFailure;

            postData.Add("donotcache", (await TimeAligner.GetSteamTimeAsync() * 1000).ToString());
            postData.Add("username", Username);
            response = await SteamWeb.RequestAsync(APIEndpoints.COMMUNITY_BASE + "/login/getrsakey", "POST", postData, cookies: cookies, proxy: Proxy).ConfigureAwait(false);
            if (response == null || response.Contains("<BODY>\nAn error occurred while processing your request."))
                return LoginResult.GeneralFailure;

            RSAResponse rsaResponse;
            try
            {
                rsaResponse = JsonConvert.DeserializeObject<RSAResponse>(response);
            }
            catch
            {
                return LoginResult.GeneralFailure;
            }

            if (!rsaResponse.Success)
            {
                return LoginResult.BadRSA;
            }

            await Task.Delay(350); //Sleep for a bit to give Steam a chance to catch up??

            byte[] encryptedPasswordBytes;
            using (RSACryptoServiceProvider rsaEncryptor = new RSACryptoServiceProvider())
            {
                byte[] passwordBytes = Encoding.ASCII.GetBytes(Password);
                RSAParameters rsaParameters = rsaEncryptor.ExportParameters(false);
                rsaParameters.Exponent = Util.HexStringToByteArray(rsaResponse.Exponent);
                rsaParameters.Modulus = Util.HexStringToByteArray(rsaResponse.Modulus);
                rsaEncryptor.ImportParameters(rsaParameters);
                encryptedPasswordBytes = rsaEncryptor.Encrypt(passwordBytes, false);
            }

            string encryptedPassword = Convert.ToBase64String(encryptedPasswordBytes);

            postData.Clear();
            postData.Add("persistence", "1");
            postData.Add("encrypted_password", encryptedPassword);
            postData.Add("account_name", Username);
            postData.Add("encryption_timestamp", rsaResponse.Timestamp);

            response = await SteamWeb.RequestAsync(APIEndpoints.BeginAuthSessionViaCredentials, "POST", postData, cookies: cookies, proxy: Proxy);
            if (response == null) return LoginResult.GeneralFailure;

            BeginAuthSessionViaCredentials.Response loginResponse;
            try
            {
                loginResponse = JsonConvert.DeserializeObject<BeginAuthSessionViaCredentials>(response)?.response;
            }
            catch
            {
                return LoginResult.GeneralFailure;
            }

            if (loginResponse == null || string.IsNullOrEmpty(loginResponse.client_id))
                return LoginResult.BadCredentials;

            if (loginResponse.allowed_confirmations?.FirstOrDefault(x => x.confirmation_type == 3) != null)
            {
                if (string.IsNullOrEmpty(TwoFactorCode))
                {
                    Requires2FA = true;
                    return LoginResult.Need2FA;
                }
            }

            if (string.IsNullOrEmpty(EmailCode) && loginResponse.allowed_confirmations != null && loginResponse.allowed_confirmations.FirstOrDefault(x => x.confirmation_type == 2) != null)
            {
                RequiresEmail = true;
                SteamID = ulong.Parse(loginResponse.steamid);
                return LoginResult.NeedEmail;
            }

            if (!string.IsNullOrEmpty(EmailCode))
            {
                postData.Clear();
                postData.Add("client_id", loginResponse.client_id);
                postData.Add("steamid", loginResponse.steamid);
                postData.Add("code_type", "2");
                postData.Add("code", EmailCode);

                response = await SteamWeb.RequestAsync(APIEndpoints.UpdateAuthSessionWithSteamGuardCode, "POST", postData, cookies: cookies, proxy: Proxy);
                if (response == null) return LoginResult.GeneralFailure;
            }

            if (!string.IsNullOrEmpty(TwoFactorCode))
            {
                postData.Clear();
                postData.Add("client_id", loginResponse.client_id);
                postData.Add("steamid", loginResponse.steamid);
                postData.Add("code_type", "3");
                postData.Add("code", TwoFactorCode);

                response = await SteamWeb.RequestAsync(APIEndpoints.UpdateAuthSessionWithSteamGuardCode, "POST", postData, cookies: cookies, proxy: Proxy);
                if (response == null) return LoginResult.GeneralFailure;
            }

            postData.Clear();
            postData.Add("client_id", loginResponse.client_id);
            postData.Add("request_id", loginResponse.request_id);

            response = await SteamWeb.RequestAsync(APIEndpoints.PollAuthSessionStatus, "POST", postData, cookies: cookies, proxy: Proxy);
            if (response == null) return LoginResult.GeneralFailure;

            PollAuthSessionStatus _PollAuthSessionStatus = JsonConvert.DeserializeObject<PollAuthSessionStatus>(response);

            if (_PollAuthSessionStatus == null || _PollAuthSessionStatus.response == null || string.IsNullOrEmpty(_PollAuthSessionStatus.response.refresh_token) || string.IsNullOrEmpty(_PollAuthSessionStatus.response.access_token))
                return LoginResult.BadCredentials;

            postData.Clear();
            postData.Add("nonce", _PollAuthSessionStatus.response.refresh_token);
            postData.Add("sessionid", cookies.GetCookies(new Uri(APIEndpoints.COMMUNITY_BASE))["sessionid"].Value);
            postData.Add("redir", "https://steamcommunity.com/login/home/?goto=");

            response = await SteamWeb.RequestAsync(APIEndpoints.FinalizeLogin, "POST", postData, cookies: cookies, proxy: Proxy);
            if (response == null) return LoginResult.GeneralFailure;

            Finalizelogin finalizeLogin = JsonConvert.DeserializeObject<Finalizelogin>(response);

            List<Finalizelogin.TransferInfo> finalizeloginTransferInfo = finalizeLogin?.transfer_info;
            if (finalizeloginTransferInfo == null || finalizeloginTransferInfo.Count == 0 || finalizeloginTransferInfo.FirstOrDefault(x => x.url == "https://steamcommunity.com/login/settoken") == null || string.IsNullOrEmpty(finalizeLogin.steamID))
                return LoginResult.GeneralFailure;

            var session = new SessionData();
            session.SteamID = ulong.Parse(loginResponse.steamid);
            session.SessionID = readableCookies["sessionid"].Value;
            session.AccessToken = _PollAuthSessionStatus.response.access_token;
            session.RefreshToken = _PollAuthSessionStatus.response.refresh_token;
            session.Proxy = Proxy;
            Session = session;
            return LoginResult.LoginOkay;
        }

        private class LoginResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("login_complete")]
            public bool LoginComplete { get; set; }

            [JsonProperty("transfer_parameters")]
            public TransferParameters TransferParameters { get; set; }

            [JsonProperty("captcha_needed")]
            public bool CaptchaNeeded { get; set; }

            [JsonProperty("captcha_gid")]
            public string CaptchaGID { get; set; }

            [JsonProperty("emailsteamid")]
            public ulong EmailSteamID { get; set; }

            [JsonProperty("emailauth_needed")]
            public bool EmailAuthNeeded { get; set; }

            [JsonProperty("requires_twofactor")]
            public bool TwoFactorNeeded { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }

        internal class TransferParameters
        {
            [JsonProperty("auth")]
            public string Auth { get; set; }

            [JsonProperty("remember_login")]
            public bool RememberLogin { get; set; }

            [JsonProperty("steamid")]
            public ulong SteamID { get; set; }

            [JsonProperty("token_secure")]
            public string SteamLoginSecure { get; set; }

            [JsonProperty("webcookie")]
            public string Webcookie { get; set; }
        }

        public class RSAResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("publickey_exp")]
            public string Exponent { get; set; }

            [JsonProperty("publickey_mod")]
            public string Modulus { get; set; }

            [JsonProperty("timestamp")]
            public string Timestamp { get; set; }

            [JsonProperty("steamid")]
            public ulong SteamID { get; set; }
        }

        public class PollAuthSessionStatus
        {
            public Response response { get; set; }

            public class Response
            {
                public string refresh_token { get; set; }
                public string access_token { get; set; }
                public bool had_remote_interaction { get; set; }
                public string account_name { get; set; }
            }
        }
        public class Finalizelogin
        {
            public string steamID { get; set; }
            public string redir { get; set; }
            public List<TransferInfo> transfer_info { get; set; }
            public string primary_domain { get; set; }

            public class TransferInfo
            {
                public string url { get; set; }
                public Params @params { get; set; }
            }

            public class Params
            {
                public string nonce { get; set; }
                public string auth { get; set; }
            }
        }
        public class BeginAuthSessionViaCredentials
        {
            public class AllowedConfirmation
            {
                public int confirmation_type { get; set; }
            }

            public class Response
            {
                public string client_id { get; set; }
                public string request_id { get; set; }
                //public object interval { get; set; }
                public List<AllowedConfirmation> allowed_confirmations { get; set; }
                public string steamid { get; set; }
                public string weak_token { get; set; }
                public string extended_error_message { get; set; }
            }

            public Response response { get; set; }
        }
    }

    public enum LoginResult
    {
        LoginOkay,
        GeneralFailure,
        BadRSA,
        BadCredentials,
        NeedCaptcha,
        Need2FA,
        NeedEmail,
        TooManyFailedLogins
    }
}
