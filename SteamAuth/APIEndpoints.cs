﻿namespace SteamAuth
{
    public static class APIEndpoints
    {
        public const string STEAMAPI_BASE = "https://api.steampowered.com";
        public const string STEAMPOWERED_BASE = "https://%s.steampowered.com";
        public const string COMMUNITY_BASE = "https://steamcommunity.com";
        public const string TWO_FACTOR_BASE = STEAMAPI_BASE + "/ITwoFactorService/%s/v0001";
        public static string TWO_FACTOR_TIME_QUERY = TWO_FACTOR_BASE.Replace("%s", "QueryTime");
        public static string HELPWIZARD_BASE = STEAMPOWERED_BASE.Replace("%s", "help");
        public static string STEAMSTORE_BASE = STEAMPOWERED_BASE.Replace("%s", "store");
        public static string LOGIN_STEAMPOWERED_BASE = STEAMPOWERED_BASE.Replace("%s", "login");

        public const string BeginAuthSessionViaCredentials = STEAMAPI_BASE + "/IAuthenticationService/BeginAuthSessionViaCredentials/v1/";
        public const string UpdateAuthSessionWithSteamGuardCode = STEAMAPI_BASE + "/IAuthenticationService/UpdateAuthSessionWithSteamGuardCode/v1/";
        public const string PollAuthSessionStatus = STEAMAPI_BASE + "/IAuthenticationService/PollAuthSessionStatus/v1/";
        public static string FinalizeLogin = LOGIN_STEAMPOWERED_BASE + "/jwt/finalizelogin";
    }
}
